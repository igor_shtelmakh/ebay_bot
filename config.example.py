# E-bay url
ebay_url = "https://www.sandbox.ebay.com/"
# Headless mode - set False if you want to see browser while doing some actions
# Set = True if you don't have graphical interface (eg on server)
headless_mode = False
