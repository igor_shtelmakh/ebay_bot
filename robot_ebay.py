import os  
import random

from selenium import webdriver  
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options  
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException

import time
import sys
from html.parser import HTMLParser

from selenium.common.exceptions import TimeoutException


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


class RobotEBay():

    ebay_url = ''
    timestamp = None

    def __init__(self, headless_mode = False, ebay_url = ''):
        self.ebay_url = ebay_url
        chrome_options = Options()  
        chrome_options.add_argument("--window-size=1024,1024")
        if headless_mode is not False:
            chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox") 
        chrome_options.add_argument("--disable-images")         
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/temp/profile")
        chrome_options.add_argument("--start-maximized");
        chrome_options.add_argument("user-agent=Mozilla/5.0 (Linux; Android 7.1.1; CPH1723 Build/N6F26Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36")
        self.driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"),   chrome_options=chrome_options) 
        self.timestamp = str(int(time.time()))

    def find_element_safe(self, selector_type, selector_value):
        element = WebDriverWait(self.driver, 3).until(
            EC.presence_of_element_located((selector_type, selector_value)),
            message = "Can't find element:" + str(selector_type) + " " + str(selector_value)
        )    
        return element

    def find_elements_safe(self, selector_type, selector_value):
        elements = WebDriverWait(self.driver, 3).until(
            EC.presence_of_all_elements_located((selector_type, selector_value)),
            message = "Can't find elements:" + str(selector_type) + " " + str(selector_value)
        )    
        return elements

    def send_keys_slowly(self, element, text, pause_ms = 0.001):
        for c in text:
            element.send_keys(c)
            time.sleep(pause_ms)

    def save_screenshot(self):
        self.timestamp = str(int(time.time()))
        self.driver.save_screenshot("screenshots/" + self.timestamp + ".png")

    def action_sign_in(self, login, password):
        try:
            self.driver.get(self.ebay_url)
            time.sleep(1)
            self.find_element_safe(By.XPATH, "//a[text()='Sign in']").click()
            time.sleep(2)
            self.send_keys_slowly(self.find_element_safe(By.XPATH, "//input[@placeholder='Email or username'][@size=40] | //input[@id='userid']"), login)
            element = self.find_element_safe(By.XPATH, "//input[@placeholder='Password'][@size=40] | //input[@id='pass']")
            self.send_keys_slowly(element, password)
            element.send_keys(Keys.ENTER);
            #self.find_element_safe(By.XPATH, "//input[@value='Sign in'] | //button[text()='Sign in']").click()
            self.save_screenshot()
            return {
                "status": True,
                "screenshot": self.timestamp
            };            
        except TimeoutException as ex:
            self.save_screenshot()
            return {
                "status": False,
                "screenshot_id": self.timestamp,
                "error": str(ex)
            };

    def action_buy_now(self, item_id, quantity = 1, options = [], finalize = False):
        try:
            url = self.ebay_url + 'itm/some/' + str(item_id)
            print(url)
            self.driver.get(url)
            time.sleep(4)
            # select options
            for option in options:
                self.find_element_safe(By.XPATH, "//select[@name='"+str(option['name'])+"']/option[text()='"+str(option['value'])+"']").click()        

            self.send_keys_slowly(self.find_element_safe(By.XPATH, "/html/body"), Keys.PAGE_DOWN)

            self.find_element_safe(By.XPATH, "//a[contains(text(),'Buy It Now')]").click()            
            time.sleep(4)
            # fill quantity
            if(quantity > 1):
                self.find_element_safe(By.XPATH, "//select[@data-test-id='CART_DETAILS_ITEM_QUANTITY']/option[text()='"+str(quantity)+"']").click()
            self.send_keys_slowly(self.find_element_safe(By.XPATH, "/html/body"), Keys.PAGE_DOWN)
            if(finalize):
                self.find_element_safe(By.XPATH, "//input[@value='Commit to buy'] | //button[@data-test-id='Confirm and pay']").click()
                time.sleep(4)
                self.find_element_safe(By.XPATH, "//input[@value='Pay now']").click()
            self.save_screenshot()
            return {
                "status": True,
                "screenshot": self.timestamp
            };            
        except TimeoutException as ex:
            self.save_screenshot()
            return {
                "status": False,
                "screenshot_id": self.timestamp,
                "error": str(ex)
            };        

    def action_place_bid(self, item_id, bid):
        try:
            url = self.ebay_url + 'itm/some/' + str(item_id)
            print(url)
            self.driver.get(url)
            time.sleep(4)
            self.send_keys_slowly(self.find_element_safe(By.XPATH, "/html/body"), Keys.PAGE_DOWN)
            self.find_element_safe(By.XPATH, "//a[contains(text(),'Place bid')]").click()            
            time.sleep(4)
            self.send_keys_slowly(self.find_element_safe(By.ID, "placebidTxtBox"), "\b\b\b\b\b\b\b\b\b\b\b" + str(bid))
            self.find_element_safe(By.XPATH, "//a[contains(text(),'Confirm Bid')]").click()  
            self.save_screenshot()
            return {
                "status": True,
                "screenshot": self.timestamp
            };            
        except TimeoutException as ex:
            self.save_screenshot()
            return {
                "status": False,
                "screenshot_id": self.timestamp,
                "error": str(ex)
            };             

    def action_make_offer(self, item_id, quantity = 1, price = 0, message="", finalize = False):
        try:
            url = self.ebay_url + 'itm/some/' + str(item_id)
            print(url)
            self.driver.get(url)
            time.sleep(4)
            self.send_keys_slowly(self.find_element_safe(By.XPATH, "/html/body"), Keys.PAGE_DOWN)
            self.find_element_safe(By.XPATH, "//a[contains(text(),'Make Offer')]").click()  
            time.sleep(4)
            self.send_keys_slowly(self.find_element_safe(By.ID, "price-input"), str(price))
            self.send_keys_slowly(self.find_element_safe(By.ID, "qty-input"), "\b\b\b" + str(quantity))
            self.send_keys_slowly(self.find_element_safe(By.ID, "msg-textarea"), str(message))
            self.find_element_safe(By.XPATH, "//button[contains(text(),'Review Offer')]").click()  
            time.sleep(4)
            if finalize:
                self.find_element_safe(By.XPATH, "//button[contains(text(),'Submit Offer')]").click() 
            time.sleep(4)
            self.save_screenshot()
            return {
                "status": True,
                "screenshot": self.timestamp
            };            
        except TimeoutException as ex:
            self.save_screenshot()
            return {
                "status": False,
                "screenshot_id": self.timestamp,
                "error": str(ex)
            };

    def leave_feedback(self, item_id, comment):
        try:
            self.find_element_safe(By.XPATH, "//button[contains(text(),'Hi ')]").click()
            time.sleep(4)            
            self.find_element_safe(By.XPATH, "//a[contains(@title,'Feedback Score')]").click()
            time.sleep(4)
            self.find_element_safe(By.XPATH, "//a[contains(text(),'Leave Feedback')]").click()
            time.sleep(4)
            #Select positive rating
            self.find_element_safe(By.XPATH, "//input[@value='positive' and contains(@id,'" + str(item_id) + "')]").click() 
            #Submit
            self.find_element_safe(By.XPATH, "//input[contains(@value,'Leave feedback')]").click() 
            #Fill feedback details
            time.sleep(4)
            self.send_keys_slowly(self.find_element_safe(By.ID, "comment00"), str(comment))
            self.find_element_safe(By.XPATH, "//a/img[contains(@alt,'Very accurate')]").click()
            self.find_element_safe(By.XPATH, "//a/img[contains(@alt,'Very quickly')]").click() 
            self.find_element_safe(By.XPATH, "//a/img[contains(@alt,'Very satisfied')]").click()
            self.find_element_safe(By.XPATH, "//input[contains(@value,'Leave feedback')]").click()

            self.save_screenshot()
            return {
                "status": True,
                "screenshot": self.timestamp
            };            
        except TimeoutException as ex:
            self.save_screenshot()
            return {
                "status": False,
                "screenshot_id": self.timestamp,
                "error": str(ex)
            };