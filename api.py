from flask import Flask, request, jsonify
from flask_cors import CORS
from robot_ebay import RobotEBay
from flask import send_file

import config

app = Flask(__name__)
CORS(app)

robot = None

@app.route('/login', methods=['GET'])
def login():
    global robot, config    
    robot = RobotEBay(config.headless_mode, config.ebay_url)
    return jsonify['status': True];
    req_data = request.get_json()
    login = req_data['login']
    password = req_data['password']
    resp = robot.action_sign_in(login, password)
    return jsonify(resp)

@app.route('/buy_now', methods=['POST'])
def buy_now():
    global robot
    req_data = request.get_json()
    item_id = req_data['item_id']
    quantity = int(req_data['quantity'])
    options = req_data['options']
    finalize = bool(req_data['finalize'])
    resp = robot.action_buy_now(item_id, quantity, options, finalize)
    return jsonify(resp)

@app.route('/place_bid', methods=['POST'])
def place_bid():
    global robot
    req_data = request.get_json()
    item_id = req_data['item_id']
    bid = req_data['bid']
    resp = robot.action_place_bid(item_id, bid)
    return jsonify(resp)

@app.route('/make_offer', methods=['POST'])
def make_offer():
    global robot
    req_data = request.get_json()
    item_id = req_data['item_id']
    quantity = req_data['quantity']
    price = req_data['price']
    message = req_data['message']
    resp = robot.action_make_offer(item_id, quantity, price, message, False)
    return jsonify(resp)

@app.route('/screenshot', methods=['GET'])
def screenshot():
    global robot, config
    id = request.args.get('id')
    path = "screenshots/" + id + ".png"
    return send_file(path, mimetype='image/png')

@app.route('/leave_feedback', methods=['POST'])
def leave_feedback():
    global robot, config
    req_data = request.get_json()
    item_id = req_data['item_id']
    comment = req_data['comment']
    resp = robot.leave_feedback(item_id, comment)
    return jsonify(resp)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=3000)
