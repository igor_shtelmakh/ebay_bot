# Installation

## 1. Install venv if it was not istalled
pip install virtualenv
virtualenv venv

## 2. Activate venv and install requirements
source ./venv/bin/activate
pip install -r requirements.txt

## 3. Install Google chrome
sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
sudo echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
sudo apt-get -y update
sudo apt-get -y install google-chrome-stable

## 4. Install Chrome driver
wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
unzip chromedriver_linux64.zip

## 5. Run API server
python api.py



# How to use

## Buy now:
POST http://server:port/buy_now
{
    "item_id": "110444224034",
    "login": "testpython@gmail.com",
    "password": "fjemba71@",
    "quantity": 2,
    "options": [
        {"name": "Model", "value": "For iPhone 6"},
        {"name": "Color", "value": "Brown"}
    ]
}

## Place bid:
POST http://server:port/place_bid
{
    "item_id": "110446297170",
    "login": "testpython@gmail.com",
    "password": "fjemba71@",
    "bid": 101
}

## Make offer:
POST http://server:port/make_offer
{
    "item_id": "110446340240",
    "login": "testpython@gmail.com",
    "password": "fjemba71@",
    "quantity": 1,
    "price": 90,
    "message": "Your message"
}

## Leave feedback:
POST http://server:port/leave_feedback
{
    "item_id": "110446340240",
    "login": "testpython@gmail.com",
    "password": "fjemba71@",
    "comment": "Your message"
}

## Watch screenshot received from response

GET http://server:port/screenshot?id={SCREENSHOT_ID}